import request from '@/utils/request'

export function getField(data) {
    return request({
        url: `/SEPR/field/getList?page=${data.pageNum}&pageSize=${data.pageSize}`,
        method: 'get',
    })
}

export function getFiledByID(id) {
    return request({
        url: `/SEPR/field/queryByID?id=${id}`,
        method: "get",
    })
}

export function getFieldByPosition(position) {
  return request({
    url: `/SEPR/field/queryByPosition?position=${position}`,
    method: "get",
  })
}

export function getFieldByType(type) {
  return request({
    url: `/SEPR/field/queryByType?type=${type}`,
    method: "get",
  })
}

export function deleteField(id) {
    return request({
        url: `/SEPR/field/deleteField/${id}`,
        method: "delete",
    })
}

export function insertField(data) {
  return request({
    url: `/SEPR//field/insertField`,
    method: "post",
    data
  })
}
export function updateField(data) {
    return request({
        url: `/SEPR/field/updateField`,
        method: "put",
        data
    })
}
