import request from '@/utils/request'

export function login(data) {
    return request({
        url: `/SEPR/admin/login`,
        method: 'post',
        data
    })
}


//管理员模块
export function addAdmin(data) {
    return request({
        url: '/SEPR/admin/addAdmin',
        method: 'post',
        data
    })
}

export function getAdminInfo(id) {
    return request({
        url: `/SEPR/admin/getAdminInfo?id=${id}`,
        method: "get",

    })
}

export function getList(data) {
    return request({
        url: `/SEPR/admin/getList?page=${data.pageNum}&pageSize=${data.pageSize}`,
        method: 'get',
    })
}


export function deleteAdmin(id) {
    return request({
        url: `/SEPR/admin/deleteAdmin/${id}`,
        method: "delete",

    })
}

export function deleteAdminRole(data) {
    return request({
        url: `/SEPR/admin/deleteAdminRole?admin_id=${data.adminId}&role_id=${data.roleId}`,
        method: "delete",
    
    })
}
export function updateAdmin(data) {
    return request({
        url: `/SEPR/admin/updateAdmin`,
        method: "put",
        data
    })
}

export function logout(token) {
    console.log("执行logout方法")
    return request({
        url: '/SEPR/admin/logout',
        method: 'get',
        // data: { "token": token }
    })
}
