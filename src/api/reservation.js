import request from '@/utils/request'

export function getReservation(data) {
    return request({
        url: `/SEPR/fieldReservation/getList?page=${data.pageNum}&pageSize=${data.pageSize}`,
        method: 'get',
    })
}

export function getReservationByID(id) {
    return request({
        url: `/SEPR/fieldReservation/queryByID?id=${id}`,
        method: "get",
    })
}

export function getReservationByDate(date) {
  return request({
    url: `/SEPR/fieldReservation/queryByPDate?date=${date}`,
    method: "get",
  })
}

export function deleteReservation(id) {
    return request({
        url: `/SEPR/fieldReservation/deleteReservation/${id}`,
        method: "delete",
    })
}

export function insertReservation(data) {
  return request({
    url: `/SEPR//fieldReservation/insertReservation`,
    method: "post",
    data
  })
}
export function updateReservation(data) {
    return request({
        url: `/SEPR/fieldReservation/updateReservation`,
        method: "put",
        data
    })
}
