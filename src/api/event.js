import request from '@/utils/request'
//课室模块
export function getList(data){
  return request({
      url:`/SEPR/event/getList`,
      method: 'get',
  })
}
export function aduitEvent(eventId) {
  return request ({
      url:`/SEPR/event/auditEvent`,
      method:"post",
      data:{
         eventId
      }
  })
}
export function rejectEvent(eventId) {
  return request ({
      url:`/SEPR/event/rejectEvent`,
      method:"post",
      data:{
         eventId
      }
  })
}

export function deleteEvent(id) {
  return request ({
      url:`/SEPR/event/deleteEvent/${id}`,
      method:"delete"
  })
}
export function updateCourse(data) {
  return request ({
      url:`/dbe/course/update`,
      method:"put",
      data
  })
}
export function insertEvent(data) {
    return request ({
      url:`/SEPR/event/insertEvent`,
      method:"post",
      data
  })
}
export function getFieldLists(status){
  return request({
    url:`/SEPR/field/queryByStatus?status=${status}`,
    method: 'get',
})
}
export function planField(data) {
  return request ({
    url:`/SEPR/event/planField`,
    method:"post",
    data
})
}
export function changeFieldStatus(id){
  return request({
    url:`/SEPR/field/updateStatus`,
    method:'put',
    data:{eventId:id}
  })
}
