import request from '@/utils/request'

//裁判通知模块

export function getJudgeNotice(data) {
  return request({
    url: `/SEPR/judge/getList?page=${data.pageNum}&pageSize=${data.pageSize}`,
    method: 'get',
  })

}

export function getJudgeByName(judgeName) {
  return request({
    url: `/SEPR/judge/getNoticeByJudgeName/${judgeName}`,
    method: 'get'
  })
}

export function deleteJudge(notice_id) {
  return request({
    url: `/SEPR/judge/deleteJudge/${notice_id}`,
    method: 'delete'
  })
}

export function insertJudge(data) {
  return request({
    url: `/SEPR//judge/insertJudgeNotice`,
    method: "post",
    data
  })
}

export function updateJudge(data){
  return request({
    url: `/SEPR/judge/updateJudge`,
    method: "put",
    data
  })
}
