import request from '@/utils/request'

export function login(data) {
    return request({
        url: `/SEPR/user/login`,
        method: 'post',
        data
    })
}


//管理员模块
export function addUser(data) {
    return request({
        url: '/SEPR/user/addUser',
        method: 'post',
        data
    })
}

export function getList(data) {
    return request({
        url: `/SEPR/user/getList?page=${data.pageNum}&pageSize=${data.pageSize}`,
        method: 'get',
    })
}

export function getUserInfo(id) {
    return request({
        url: `/SEPR/user/getUserInfo?id=${id}`,
        method: "get",

    })
}


export function deleteUser(id) {
    return request({
        url: `/SEPR/user/deleteUser/${id}`,
        method: "delete",

    })
}
export function updateUser(data) {
    return request({
        url: `/SEPR/user/updateUser`,
        method: "put",
        data
    })
}
