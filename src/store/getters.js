const getters = {
  routes: state => state.permission.routes,
  updatedUserRouters: state => state.update.hasUpdateUserRouter,
  updatedRouters: state => state.update.hasUpdateRouter,
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  roles: state => state.user.roles
}
export default getters
