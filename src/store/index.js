import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import admin from './modules/admin'
import update from './modules/update'
import permission from './modules/permission'
Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        permission,
        admin,
        update,
        app,
        settings,
        user
    },
    getters
})

export default store