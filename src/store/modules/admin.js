import {  logout } from '@/api/admin'
const state = {
    tokenID: "",
    adminName: "管理员",
}

const mutations = {
    SET_TOKENID: (state, tokenID) => {
        state.tokenID = tokenID;
    },
    SET_ADMINNAME: (state, adminName) => {
        console.log("我执行了")
        state.adminName = adminName;
    }
}

const actions = {
    setTokenID({ commit }, tokenID) {
        commit('SET_TOKENID', tokenID)
    },
    setAdminName({ commit }, adminName) {
        commit('SET_ADMINNAME', adminName)
    },
    // admin logout
    logout({ commit, state }) {
        console.log("执行logout")
        return new Promise((resolve, reject) => {
            logout(state.token).then(() => {
                commit('SET_TOKENID', '')
                removeToken()
                resetRouter()
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
}