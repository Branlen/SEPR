

const state = {
   hasUpdateRouter:false,
    hasUpdateUserRouter: false,
}

const mutations = {
    SET_UPDATEROUTER: (state, hasUpdateRouter) => {
        state.hasUpdateRouter = hasUpdateRouter;
        console.log('===退出更新hasRouter',state.hasUpdateRouter)
    
    },
    SET_UPDATEUSERROUTER: (state, hasUpdateUserRouter) => {
        state.hasUpdateUserRouter = hasUpdateUserRouter;
        console.log('===退出更新hasUpdateUserRouter', state.hasUpdateUserRouter)

    }
}


const actions = {
    updateRouter({ commit },isTrue){
        commit('SET_UPDATEROUTER', isTrue)
    },
    updateUserRouter({ commit }, isTrue) {
        commit('SET_UPDATEUSERROUTER', isTrue)
    }
}


export default {
    namespaced: true,
    state,
    mutations,
    actions
}