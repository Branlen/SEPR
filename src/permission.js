import router from './router'
import { asyncRoutes,asyncUserRoutes,constantRoutes } from './router/index'
import store from './store/index'
import {
  Message
} from 'element-ui'

import {
  getToken
} from '@/utils/auth' // get token from cookie
import { getUserID, removeToken, removeUserID } from './utils/auth'




const whiteList = ['/login', '/mobile/login'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  console.log("===路由前置守卫===");


  // determine whether the user has logged in
  const hasToken = getToken();

  const hasUseId = getUserID();

  console.log('hasToken')
  console.log(hasToken)
  console.log('hasUserId')
  console.log(hasUseId)
  if (hasToken) {
    if (hasUseId) {
      const hasUpadteUserRouter = store.getters.updatedUserRouters;
      console.log("==hasUpadteUserRouter==")
      console.log(hasUpadteUserRouter);
      if (to.path === '/mobile/login') {
        console.log("===我已登录===");
        // if is logged in, redirect to the home page
        next({
          path: '/mobile/home'
        })

      } else {
        //判断是否已经更新router？？？？？
        // determine whether the user has obtained his permission roles through getInfo
        if (hasUpadteUserRouter) {
          console.log("路由已更新")
          // console.log(this.$router.options.routes);
          next();
          console.log("跳转路由完毕")
        } else {
          console.log("路由没有更新，尝试获取角色更新")
          try {
            // get user info
            // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
            // 获得roles角色
            // const roles = store.getters.roles

            // // generate accessible routes map based on roles
            await store.dispatch('permission/myGenerateUserRoutes')

            // dynamically add accessible routes
            await store.dispatch('update/updateUserRouter', true)
            router.addRoutes(asyncUserRoutes)

            // hasUpadteRouter = true;
            // hack method to ensure that addRoutes is complete
            // set the replace: true, so the navigation will not leave a history record
            next({
              ...to,
              replace: true
            })
          } catch (error) {
            //报错或token过期，删除token并重新登录
            // remove token and go to login page to re-login
            // removeToken();
            // removeUserID();
            console.log(error)
            next(`/mobile/login?redirect=${to.path}`)

          }
        }
      }

    }
    else {
      const hasUpadteRouter = store.getters.updatedRouters
      console.log("==hasUpdateRouter==")
      console.log(hasUpadteRouter);
      if (to.path === '/login') {
        console.log("===我已登录===");
        // if is logged in, redirect to the home page
        next({
          path: '/'
        })

      } else {
        //判断是否已经更新router？？？？？
        // determine whether the user has obtained his permission roles through getInfo
        if (hasUpadteRouter) {
          console.log("路由已更新")
          // console.log(this.$router.options.routes);
          next()
        } else {
          console.log("路由没有更新，尝试获取角色更新")
          try {
            // get user info
            // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
            // 获得roles角色
            // const roles = store.getters.roles

            // // generate accessible routes map based on roles
            await store.dispatch('permission/myGenerateRoutes')

            // dynamically add accessible routes
            await store.dispatch('update/updateRouter', true)
            router.addRoutes(asyncRoutes)

            // hasUpadteRouter = true;
            // hack method to ensure that addRoutes is complete
            // set the replace: true, so the navigation will not leave a history record
            next({
              ...to,
              replace: true
            })
          } catch (error) {
            //报错或token过期，删除token并重新登录
            // remove token and go to login page to re-login
            await store.dispatch('user/resetToken')
            Message.error(error || 'Has Error')
            next(`/login?redirect=${to.path}`)

          }
        }
      }


    }
  }
  else {
    /* has no token*/

    //进入登录页面 ，不需要token
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)

    }
  }



})

router.afterEach(() => {

})
