
import Vue from 'vue'
import Router from 'vue-router'

import login from '@/pages/login/login'
import register from '@/pages/login/register'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import MobileLayout from '@/mobile-layout'

/**
 * 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * hidden: true // (默认 false)
 *
 * 当设置 noredirect 的时候该路由在面包屑导航中不可被点击
 * redirect: 'noredirect'
 *
 * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 * 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 * 若你想不管路由下面的 children 声明的个数都显示你的根路由
 * 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * alwaysShow: true
 * name: 'router-name' //设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta: {
 * roles: ['admin', 'editor'] //设置该路由进入的权限，支持多个权限叠加
 * title: 'title' //设置该路由在侧边栏和面包屑中展示的名字
 * icon: 'svg-name' //设置该路由的图标
 * noCache: true //如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
 * breadcrumb: false // 如果设置为false，则不会在breadcrumb面包屑中显示
 * }
 */


/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */

export const constantRoutes = [
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    children: [{
      path: 'login',
      name: 'userLogin',
      component: () => import('@/pages/mobile/user-login/index'),
      meta: {
        title: '用户登录'
      }
    }]
  },{
  path: '/login',
  name: 'login',
  component: login
},
{
  path: '/register',
  name: 'register',
  component: register
},
{
  path: '/forget',
  name: 'forget',
  component: () => import('@/pages/login/forget')
},

{
  path: '/404',
  component: () => import('@/pages/404'),
  hidden: true
},
  //  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

/**
* asyncRoutes
* the routes that need to be dynamically loaded based on user roles
*/
export const asyncRoutes = [


  {
    path: '/',
    component: Layout,
    redirect: '/certification',
    children: [{
      path: 'certification',
      name: 'Certification',
      component: () => import('@/pages/home/index'),
      meta: {
        title: '首页',
        icon: 'home'
      }
    }]
  },
  {
    path: '/fieldManager',
    component: Layout,
    redirect: '/fieldManager/index',
    children: [{
      path: "index",
      component: () => import('@/pages/field/index'),
      name: 'fieldManager',
      meta: {
        title: '场地管理',
        icon: 'dashboard'
      }
    }]
  },
  {
    path: '/reservationManager',
    component: Layout,
    redirect: '/reservationManager/index',
    children: [{
      path: "index",
      component: () => import('@/pages/reservation/index'),
      name: 'reservationManager',
      meta: {
        title: '预约管理',
        icon: 'dashboard'
      }
    }]
  },
  {
    path: '/equipmentManager',
    component: Layout,
    redirect: '/equipmentManager/index',
    children: [{
      path: "index",
      component: () => import('@/pages/equipment/index'),
      name: 'equipmentManager',
      meta: {
        title: '器材管理',
        icon: 'link'
      }
    }]
  },
  {
    path: '/eventManager',
    component: Layout,
    redirect: '/eventManager/index',
    children: [{
      path: "index",
      component: () => import('@/pages/event/index'),
      name: 'eventManager',
      meta: {
        title: '赛事管理',
        icon: 'tree'
      }
    }]
  }, {
    path: '/judgeNoticeManager',
    component: Layout,
    redirect: '/judgeNotice/index',
    children: [{
      path: "index",
      component: () => import('@/pages/judgeNotice/index'),
      name: 'judgeNoticeManager',
      meta: {
        title: '裁判公告',
        icon: 'msg'
      }
    }]
  },
  {
    path: '/userManager',
    component: Layout,
    redirect: '/user/index',
    children: [{
      path: "index",
      component: () => import('@/pages/user/index'),
      name: 'userManager',
      meta: {
        title: '用户管理',
        icon: 'msg'
      }
    }]
  },
  {
    path: '/adminManager',
    component: Layout,
    redirect: '/admin/index',
    children: [{
      path: "index",
      component: () => import('@/pages/admin/index'),
      name: 'adminManager',
      meta: {
        title: '管理员管理',
        icon: 'msg'
      }
    }]
  },

  {
    path: '/404',
    component: () => import('@/pages/404'),
    hidden: true
  },



  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

export const asyncUserRoutes =[
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    redirect: '/mobile/home',
    children: [{
      path: 'home',
      name: '首页',
      component: () => import('@/pages/mobile/home/index'),
      meta: {
        title: '首页'
      }
    }]
  },
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    children: [{
      path: 'myEvent',
      name: '我的赛事预约',
      component: () => import('@/pages/mobile/my-event/index'),
      meta: {
        detail: true,
        title: '我的赛事预约'

      }
    }]
  },
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    children: [{
      path: 'newEvent',
      name: '新建预约',
      component: () => import('@/pages/mobile/create-event/index'),
      meta: {
        detail: true,
        title: '新建预约'

      }
    }]
  },
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    children: [{
      path: 'newFieldAppoint',
      name: '新建场地预约',
      component: () => import('@/pages/mobile/create-field-appoint/index'),
      meta: {
        detail: true,
        title: '新建场地预约'

      }
    }]
  },
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    children: [{
      path: 'checkField',
      name: '查看场地信息',
      component: () => import('@/pages/mobile/check-field/index'),
      meta: {
        detail: true,
        title: '查看场地信息'

      }
    }]
  },
  {
    path: '/mobile',
    component: MobileLayout,
    name: 'shouye',
    children: [{
      path: 'myField',
      name: '我的场地预约',
      component: () => import('@/pages/mobile/my-field/index'),
      meta: {
        detail: true,
        title: '我的场地预约'

      }
    }]
  },
  {
    path: '/404',
    component: () => import('@/pages/404'),
    hidden: true
  },



  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }

]
export default router
